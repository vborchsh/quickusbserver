
#include "main.h"

int main(int argc, char *argv[])
{
  QCoreApplication a(argc, argv);

  printf("//---------------------------------------------------------------");
  printf("\r\n");
  printf("// Quick USB TCP server ver. %s", pAPP_VERSION);
  printf("\r\n");
  printf("// Contacts: Vladislav Borchsh (vladislav.borchsh@cern.ch)");
  printf("\r\n");
  printf("//---------------------------------------------------------------");
  printf("\r\n");

  // CLI port definition
  if (argc > 2) {
    if (NULL == strncmp(argv[1], "--port", 6)) {
      m_port = atoi(argv[2]);
      printf("Port definined: %d", m_port);
      printf("\r\n");
    }
    else {
      printf("Unknown argument: ");
      printf(argv[1]);
      printf("\r\n");
      printf("Finishing...");
      printf("\r\n");
      return -1;
    }
  }
  else {
    // Hand port definition
    printf("Enter port number:");
    printf("\r\n");
    while (scanf("%d", &m_port) != 1) {
      scanf("%*s");
      printf("Enter port number:");
      printf("\r\n");
    }
  }

  m_server = new qusbserver(nullptr);
  m_server->RunServer(m_port);

  return a.exec();
}
