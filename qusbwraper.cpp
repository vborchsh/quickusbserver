
#include "qusbwraper.h"

qusbwraper::qusbwraper()
{
  char module_descriptor[256];
  unsigned short minor, major, build;
  unsigned long err_code;

  if (0 == Find_Modules()) {
    printf("No modules found");
  }
  printf("\r\n");

  if (QuickUsbGetStringDescriptor(hDevice, 2, module_descriptor, 256) == 0) {
    QuickUsbGetLastError(&err_code);
    printf("Unable to check QUSB descriptor, error code %ld", err_code);
  }
  else {
    printf ("%s", module_descriptor);
  }
  printf("\r\n");

  if (QuickUsbGetDriverVersion(&major, &minor, &build)==0) {
    QuickUsbGetLastError(&err_code);
    printf("Unable to check QUSB driver version, error code %ld", err_code);
  }
  else {
    printf ("QUSB driver version: %d.%d.%d", major, minor, build);
  }
  printf("\r\n");

  if (QuickUsbGetDllVersion(&major, &minor, &build)==0) {
    QuickUsbGetLastError(&err_code);
    printf("Unable to check qusb libraries version, error code %ld", err_code);
  }
  else {
    printf ("QUSB libraries version: %d.%d.%d", major, minor, build);
  }
}


long qusbwraper::Find_Modules(void)
{
  char *str, devname[20], namelist[512];
  unsigned int lenght=512;

  QuickUsbFindModules(namelist, lenght);
  str=namelist;

  while(*str!='\0'){
    strncpy(devname,str,20);
    str+=strlen(str);
  }

  return QuickUsbOpen(&hDevice,devname);
}


long qusbwraper::Close_Device(void)
{
  return QuickUsbClose(hDevice);
}


long qusbwraper::RSettings(unsigned short *data)
{
  unsigned short iverb = 0;
  return QuickUsbReadSetting(hDevice, iverb, data);
}


long qusbwraper::WSettings(unsigned short data)
{
  unsigned short iverb = 0;
  return QuickUsbWriteSetting(hDevice, iverb, data);
}


long qusbwraper::RDefault(unsigned short *data)
{
  unsigned short iverb = 0;
  return QuickUsbReadDefault(hDevice, iverb, data);
}


long qusbwraper::WDefault(unsigned short data)
{
  unsigned short iverb = 0;
  return QuickUsbWriteDefault(hDevice, iverb, data);
}


long qusbwraper::I2C_Read(unsigned short* data, unsigned short addr, unsigned short length)
{
  long result;
  result=QuickUsbReadI2C(hDevice, (QWORD)addr, (QBYTE*)data, (QWORD*)&length);
  return result;
}


long qusbwraper::I2C_Write(unsigned short* data, unsigned short addr, unsigned short length)
{
  long result;
  result = QuickUsbWriteI2C(hDevice, (QWORD)addr, (QBYTE*)data, (QWORD)length);
  return result;
}


unsigned long qusbwraper::QUSB_LastError(void)
{
  unsigned long error_id;

  QuickUsbGetLastError(&error_id);

  return error_id;
}


long qusbwraper::Port_RDir(unsigned short port, unsigned short *data)
{
  unsigned short addr;
  long res;

  if ((port < 0x0A) || (port > 0x0E)) {
    res = -1;
  }
  else {
    addr = port-10;
    res = QuickUsbReadPortDir(hDevice, (QWORD)addr, (QBYTE*)data);
  }
  return res;
}


long qusbwraper::Port_WDir(unsigned short port, unsigned short data)
{
  unsigned short addr;
  long res;

  if ((port < 0x0A) || (port > 0x0E)) {
    res = -1;
  }
  else {
    addr = port-10;
    res = QuickUsbWritePortDir(hDevice, static_cast<QWORD>(addr), static_cast<QBYTE>(data));
  }
  return res;
}


long qusbwraper::Port_Read(unsigned short port, unsigned short *data)
{
  unsigned short addr, length;
  long res;

  if ((port < 0x0A) || (port > 0x0E)) {
    res = -1;
  }
  else {
    addr=port-10;
    res = QuickUsbReadPort(hDevice, addr, (QBYTE*)data, &length);
  }
  return res;
}


long qusbwraper::Port_Write(unsigned short port, unsigned short data)
{
  unsigned short addr;
  long res;

  if ((port < 0x0A) || (port > 0x0E)) {
    res = -1;
  }
  else {
    addr = port-10;
    res = QuickUsbWritePort(hDevice, addr, (QBYTE*)&data, 1);
  }
  return res;
}


long qusbwraper::Command_Read(unsigned short *data, unsigned short addr)
{
  unsigned short length2 = 2;
  return QuickUsbReadCommand(hDevice, (QWORD)addr, (QBYTE*)data, (QWORD*)&length2);
}


long qusbwraper::Command_Write(unsigned short *data, unsigned short addr)
{
  unsigned short length2 = 2;
  return QuickUsbWriteCommand(hDevice, (QWORD)addr, (QBYTE*)data, (QWORD)length2);
}


long qusbwraper::Data_Read(unsigned long words, unsigned char* buf)
{
  unsigned short address = 0x8000; // non incremental address
  long res = 0;

  if (!QuickUsbWriteSetting(hDevice, 2, address)) {
    res = -1;
  }
  else {
    res = QuickUsbReadData(hDevice, (PQBYTE)buf, (PQULONG)&words);
  }

  return res;
}

