#ifndef QUSBSERVER_H
#define QUSBSERVER_H

#include <QTcpServer>
#include <QTcpSocket>
#include <QElapsedTimer>

#include "qusbwraper.h"

extern QHANDLE hDevice;

class qusbserver : public QTcpServer
{
  Q_OBJECT
public:
  qusbserver(QTcpServer *parent = nullptr);
  ~qusbserver();

  void            RunServer(unsigned short port);
  void            StopServer();
  qint64          GetElapsedTime();

private:
  qusbwraper*     m_qusb;
  QTcpSocket*     m_socket;
  QElapsedTimer*  m_uptimer;
  void            reqHandler(QByteArray req);

private slots:
  void            slotNewConnection();
  void            slotReadClient();
  void            slotClientDisconnected();
  void            bytesWritten(qint64 cnt);
};

#endif // QUSBSERVER_H
