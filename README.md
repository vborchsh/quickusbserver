# TCP server for QuickUSB module library
<p>...</p>

## Rules
<p>1. One packet - one command.</p>
<p>2. You should be ready readback response, after you send a command.</p>
<p>3. Socket automatically closed after you break connection.</p>
<p>...</p>

## Command list
### Default QUSB library commands:
<p>"FIND" - Find QUSB modules, connected to host-pc;</p>
<p>"CLOSE" - Close current QUSB module;</p>
<p>"RSET" - not fully implemented;</p>
<p>"WSET" - not fully implemented;</p>
<p>"RDEF" - not fully implemented;</p>
<p>"WDEF" - not fully implemented;</p>
<p>"RI2C" - Read I2C register. Format of command: "RI2C/ADDRESS". Currently can be used only for one register. ADDRESS should be in HEX.</p>
<p>"WI2C" - Write I2C register. Format of command: "RI2C/ADDRESS/DATA". Currently can be used only for one register. ADDRESS and DATA should be in HEX</p>
<p>"LERR" - Last error form QUSB module. Return a digits-code of error.</p>
<p>"RCMD" - Read command register. Format of command: "RCMD/ADDRESS". ADDRESS should be in HEX.</p>
<p>"WCMD" - Write command register. Format of command: "WCMD/ADDRESS/DATA". ADDRESS should be in HEX.</p>
<p>"RDAT" - Read a data stream from highspeed parallel port. Format of command: "WCMD/SIZE". SIZE should be in DEC [bytes].</p>

### Other commands:
<p>"UPTIME" - Return a 64bit of uptime server in ms.</p>
