QT -= gui
QT += network

CONFIG += c++11 console
CONFIG -= app_bundle

QMAKE_CFLAGS   += -c
QMAKE_CFLAGS   += -g

QMAKE_CXXFLAGS += -O
QMAKE_CXXFLAGS += -Wall
QMAKE_CXXFLAGS += -pthread
QMAKE_CXXFLAGS += -c

# Should be correct path to QUSB library
win32 {
  QMAKE_LFLAGS += C:\Windows\System32\QuickUSB.dll -lws2_32
}
unix {
  QMAKE_LFLAGS += /opt/quickusb/quickusb
}
mac {
  #QMAKE_LFLAGS +=
}

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES +=  main.cpp       \
            qusbserver.cpp \
            qusbwraper.cpp

HEADERS +=  main.h         \
            QuickUSB.h     \
            qusbserver.h   \
            qusbwraper.h
