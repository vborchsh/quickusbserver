#ifndef MAIN_H
#define MAIN_H

#include <QCoreApplication>
#include <stdio.h>

#include "qusbserver.h"

#define pAPP_VERSION "0.1.4"

QHANDLE         hDevice;
unsigned short  m_port;
qusbserver*     m_server;

#endif // MAIN_H
