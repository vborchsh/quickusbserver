#ifndef QUSBWRAPER_H
#define QUSBWRAPER_H

#include <stdio.h>
#include <string.h>
#include "QuickUSB.h"

extern QHANDLE hDevice;

class qusbwraper
{
public:
  explicit          qusbwraper();

  long              Find_Modules(void);
  long              Close_Device(void);
  long              RSettings(unsigned short *data);
  long              WSettings(unsigned short data);
  long              RDefault(unsigned short *data);
  long              WDefault(unsigned short data);
  long              I2C_Read(unsigned short *data, unsigned short addr, unsigned short length = 2);
  long              I2C_Write(unsigned short *data, unsigned short addr, unsigned short length = 1);
  unsigned long     QUSB_LastError(void);
  long              Port_RDir(unsigned short port, unsigned short *data);
  long              Port_WDir(unsigned short port, unsigned short data);
  long              Port_Read(unsigned short port, unsigned short *data);
  long              Port_Write(unsigned short port, unsigned short data);
  long              Command_Read(unsigned short *data, unsigned short addr);
  long              Command_Write(unsigned short *data, unsigned short addr);
  long              Data_Read(unsigned long words, unsigned char *buf);
};

#endif // QUSBWRAPER_H
