
#include "qusbserver.h"


qusbserver::qusbserver(QTcpServer *parent)
{
  Q_UNUSED(parent)

  printf("//---------------------------------------------------------------");
  printf("\r\n");
  printf("// Creating server...");
  printf("\r\n");
  m_uptimer = new QElapsedTimer();
  m_socket = new QTcpSocket(this);
  connect(this, SIGNAL(newConnection()), this, SLOT(slotNewConnection()));
  printf("Done!");
  printf("\r\n");

  printf("// Update USB information...");
  m_qusb = new qusbwraper();
  printf("\r\n");
  printf("Done!");
  printf("\r\n");
  printf("//---------------------------------------------------------------");
  printf("\r\n");
}


qusbserver::~qusbserver()
{
  this->StopServer();
}


qint64 qusbserver::GetElapsedTime()
{
  return m_uptimer->elapsed();
}


void qusbserver::RunServer(unsigned short port)
{
  bool res = this->listen(QHostAddress::Any, port);

  if (res == true) {
    printf("Server is up. Port [%d]", port);
    printf("\r\n");
    m_uptimer->restart();
  }
  else {
    printf("!!! Can't run server, port [%d]. Try again...", port);
    printf("\r\n");
  }
  printf("//---------------------------------------------------------------");
  printf("\r\n");
}


void qusbserver::StopServer()
{
  printf("Uptime: %lld", GetElapsedTime());
  printf("\r\n");

  if (true == m_socket->isOpen()) {
    m_socket->close();
  }

  if (true == this->isListening()) {
    this->close();
  }
}


void qusbserver::slotNewConnection()
{
  m_socket = this->nextPendingConnection();
  connect(m_socket, SIGNAL(readyRead())         , this, SLOT(slotReadClient())        );
  connect(m_socket, SIGNAL(disconnected())      , this, SLOT(slotClientDisconnected()));
  connect(m_socket, SIGNAL(bytesWritten(qint64)), this, SLOT(bytesWritten(qint64))    );

  // Welcome message
  QByteArray dat("You are connected to DIGITIZER Quick USB TCP server");
  m_socket->write(dat);
  m_socket->waitForBytesWritten();

  QString str = "New client connected. Address: " + m_socket->localAddress().toString() + "Port: " + QString().sprintf("%d", m_socket->localPort());
  printf(str.toStdString().c_str());
  printf("\r\n");
}


void qusbserver::slotReadClient()
{
  QByteArray arr = m_socket->readAll();

  printf("Readed from client:");
  printf("\r\n");
  printf(arr.toStdString().c_str());
  printf("\r\n");

  // Request handler
  reqHandler(arr);
}


void qusbserver::slotClientDisconnected()
{
  if (m_socket->isOpen()) {
    m_socket->close();
  }
  printf("Cient is disconnected");
  printf("\r\n");
}


void qusbserver::bytesWritten(qint64 cnt)
{
  QString str = "Send to client: " + QString().sprintf("%lld", cnt) + " bytes";
  printf(str.toStdString().c_str());
  printf("\r\n");
}


void qusbserver::reqHandler(QByteArray req)
{
  QString resp;
  QString str(req);
  QStringList list = str.split('/'); //qDebug() << list << "size" << list.size();
  QByteArray buf;
  unsigned short addr, data;
  long lr = 0;

  // We can't use switch-case for strings
  if (list.contains("FIND")) {
    lr = m_qusb->Find_Modules();
    resp = "FIND: " + QString().sprintf("%ld", lr);
  }
  else if (list.contains("CLOSE")) {
    lr = m_qusb->Close_Device();
    resp = "CLOSE: " + QString().sprintf("%ld", lr);
  }
  else if (list.contains("RSET"))  {
    lr = m_qusb->RSettings(&data);
    if (!lr) {
      resp = "RSET: Error";
    }
    else {
      resp = "RSET: " + QString().sprintf("%u", data);
    }
  }
  else if (list.contains("WSET"))  {
    data = 0xAA; // ???
    lr = m_qusb->WSettings(data);
    if (!lr) {
      resp = "WSET: Error";
    }
    else {
      resp = "WSET: " + QString().sprintf("%u", data);
    }
  }
  else if (list.contains("RDEF"))  {
    lr = m_qusb->RDefault(&data);
    if (!lr) {
      resp = "RDEF: Error";
    }
    else {
      resp = "RDEF: " + QString().sprintf("%u", data);
    }
  }
  else if (list.contains("WDEF"))  {
    data = 0xAA; // ???
    lr = m_qusb->WDefault(data);
    if (!lr) {
      resp = "WDEF: Error";
    }
    else {
      resp = "WDEF: " + QString().sprintf("%u", data);
    }
  }
  else if (list.contains("RI2C"))  {
    if (list.size() < 2) {
      resp = "RI2C: Incorrect telegram format!";
    }
    else {
      addr = list.at(1).toUShort(nullptr, 16);
      data = 0;
      long res = m_qusb->I2C_Read(&data, addr, 2);
      if (!res) {
        resp = "RI2C is failed!";
      }
      else {
        resp = "RI2C: " + QString().sprintf("Addr: 0x%X Data: 0x%X(%d)", addr, data, data);
      }
    }
  }
  else if (list.contains("WI2C"))  {
    if (list.size() < 3) {
      resp = "WI2C: Incorrect telegram format!";
    }
    else {
      addr = list.at(1).toUShort(nullptr, 16);
      data = list.at(2).toUShort(nullptr, 16);
      long res = m_qusb->I2C_Write(&data, addr, 1);
      if (!res) {
        resp = "WI2C is failed!";
      }
      else {
        resp = "WI2C: " + QString().sprintf("Addr: 0x%X Data: 0x%X(%d)", addr, data, data);
      }
    }
  }
  else if (list.contains("LERR"))  {
    lr = static_cast<long>(m_qusb->QUSB_LastError());
    if (!lr) {
      resp = "LERR: Error";
    }
    else {
      resp = "LERR: " + QString().sprintf("%ld", lr);
    }
  }
  else if (list.contains("RDIR"))  {
    if (list.size() < 2) {
      resp = "RDIR: Incorrect telegram format!";
    }
    else {
      addr = list.at(1).toUShort(nullptr, 16);
      lr = m_qusb->Port_RDir(addr, &data);
      if (!lr) {
        resp = "RDIR: Error";
      }
      else {
        resp = "RDIR: " + QString().sprintf("Port: 0x%X; Dir: 0x%X", addr, data);
      }
    }
  }
  else if (list.contains("WDIR"))  {
    if (list.size() < 3) {
      resp = "WDIR: Incorrect telegram format!";
    }
    else {
      addr = list.at(1).toUShort(nullptr, 16);
      data = list.at(2).toUShort(nullptr, 16);
      lr = m_qusb->Port_WDir(addr, data);
      if (!lr) {
        resp = "WDIR: Error";
      }
      else {
        resp = "WDIR: " + QString().sprintf("Port: 0x%X; Dir: 0x%X", addr, data);
      }
    }
  }
  else if (list.contains("PRD"))   {
    if (list.size() < 2) {
      resp = "PRD: Incorrect telegram format!";
    }
    else {
      addr = list.at(1).toUShort(nullptr, 16);
      lr = m_qusb->Port_Read(addr, &data);
      if (!lr) {
        resp = "PRD: Error";
      }
      else {
        resp = "PRD: " + QString().sprintf("Port: 0x%X; Data: 0x%X", addr, data);
      }
    }
  }
  else if (list.contains("PWR"))   {
    if (list.size() < 3) {
      resp = "PWR: Incorrect telegram format!";
    }
    else {
      addr = list.at(1).toUShort(nullptr, 16);
      data = list.at(2).toUShort(nullptr, 16);
      lr = m_qusb->Port_Write(addr, data);
      if (!lr) {
        resp = "PWR: Error";
      }
      else {
        resp = "PWR: " + QString().sprintf("Port: 0x%X; Data: 0x%X", addr, data);
      }
    }
  }
  else if (list.contains("RCMD"))  {
    if (list.size() < 2) {
      resp = "RCMD: Incorrect telegram format!";
    }
    else {
      addr = list.at(1).toUShort(nullptr, 16);
      data = 0;
      long res = m_qusb->Command_Read(&data, addr);
      if (!res) {
        resp = "RCMD is failed!";
      }
      else {
        resp = "RCMD: " + QString().sprintf("Addr: %x Data: %x(%d)", addr, data, data);
      }
    }
  }
  else if (list.contains("WCMD"))  {
    if (list.size() < 3) {
      resp = "WCMD: Incorrect telegram format!";
    }
    else {
      addr = list.at(1).toUShort(nullptr, 16);
      data = list.at(2).toUShort(nullptr, 16);
      long res = m_qusb->Command_Write(&data, addr);
      if (!res) {
        resp = "WCMD is failed!";
      }
      else {
        resp = "WCMD: " + QString().sprintf("Addr: %x Data: %x(%d)", addr, data, data);
      }
    }
  }
  else if (list.contains("RDAT"))  {
    if (list.size() < 2) {
      resp = "RDAT: Incorrect telegram format!";
    }
    else {
      unsigned long words = list.at(1).toULong();
      unsigned char rdat_buf [2*words];
      lr = m_qusb->Data_Read(words, rdat_buf);

      if (lr != 0) {
        resp.append(QString().sprintf("RDAT: %ld words successfully readout\r\n", words/2));
        for (int i = 0; i < words/2; i++) {
          if ((i % 16 == 0) & (i > 1)) {
            resp.append(QString().sprintf(":[0x%02X]\r\n", i));
          }
          resp.append(QString().sprintf(" %02X%02X ", rdat_buf[i*2+1], rdat_buf[i*2]));
        }
      }
      else {
        resp.append(QString().sprintf("RDAT: Unable to complete data transfer\r\n"));
        for (int i = 0; i < words/2; i++) {
          if ((i % 16 == 0) & (i > 1)) {
            resp.append(QString().sprintf(":[0x%02X]\r\n", i));
          }
          resp.append(QString().sprintf(" %02X%02X ", rdat_buf[i*2+1], rdat_buf[i*2]));
        }
      }
    }
  }
  else if (list.contains("UPTIME")) {
    resp.append(QString().sprintf("UPTIME: %lld", GetElapsedTime()));
  }
  else {
    resp = "Unknown telegram";
  };

  buf = resp.toUtf8();
  this->m_socket->write(buf);
}
